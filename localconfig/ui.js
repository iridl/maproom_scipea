var uicoreConfig = uicoreConfig || {};
/* set google Analytics Id */
uicoreConfig.GoogleAnalyticsId='UA-91395916-4';
/* enable helpmail button */
uicoreConfig.helpemail='help@iri.columbia.edu';
/* enable GoogleSearch */
/* uicoreConfig.GoogleSearchCX='00....'; */
uicoreConfig.resolutionQueryServers["default"]= "/";
