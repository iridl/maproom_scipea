/* to change the GoogleAnalyticsId, redefine this after udunits.js */
uicoreConfig.GoogleAnalyticsId = '91395916-4';
uicoreConfig.helpemail = 'help@iri.columbia.edu';

/* additional default uicoreConfig settings */
/*uicoreConfig.resolutionQueryServers["default"]= "http://map.meteomadagascar.mg/";*/
/*uicoreConfig.resolutionQueryServers["default"]= "/";*/
uicoreConfig.resolutionQueryServers["default"] = "/";
/*uicoreConfig.resolutionQueryServers["irids:SOURCES:Features:Political:World:Countries:ds"]= "http://iridl.ldeo.columbia.edu/";*/
/*uicoreConfig.resolutionQueryServers["irids:SOURCES:Features:Political:Madagascar:Regions:ds"]= "http://iridl.ldeo.columbia.edu/";*/
/*uicoreConfig.resolutionQueryServers["irids:SOURCES:Features:Political:Madagascar:Districts:ds"]= "http://iridl.ldeo.columbia.edu/";*/
/*uicoreConfig.resolutionQueryServers["irids:SOURCES:Madagascar:Forecast:rainfall:Jul_rr:ds"]= "http://map.meteomadagascar.mg/";*/
/*uicoreConfig.resolutionQueryServers["irids:SOURCES:Madagascar:Forecast:temperature:Jul_tt:ds"]= "http://map.meteomadagascar.mg/";*/
